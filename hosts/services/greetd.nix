{
  config,
  lib,
  ...
}: {
  services.greetd = let
    session = {
      command = "${lib.getExe config.programs.hyprland.package}";
      user = "bitthal";
    };
  in {
    enable = true;
    settings = {
      terminal.vt = 1;
      default_session = session;
      initial_session = session;
    };
  };

  # Enable hyprlock pam
  security.pam.services.hyprlock = {};

  # Unlock GPG keyring
  security.pam.services.greetd.enableGnomeKeyring = true;
}
