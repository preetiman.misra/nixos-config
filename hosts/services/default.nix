{
  imports = [
    ./greetd.nix
    ./printing.nix
    ./docker.nix
  ];

  # flatpak
  services.flatpak.enable = true;
}
