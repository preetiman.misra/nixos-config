{pkgs, ...}: {
  services = {
    # Printing configuration
    printing = {
      enable = true;
      drivers = with pkgs; [
        gutenprint
        hplip
        hplipWithPlugin
        cnijfilter2

        (pkgs.callPackage ../../package/konicaDriver.nix {})
      ];
    };

    # Network autodiscovery
    avahi = {
      enable = true;
      nssmdns4 = true;
      openFirewall = true;
    };
  };
}
