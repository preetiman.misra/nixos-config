{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./configuration.nix
  ];

  console = {
    useXkbConfig = true;
    earlySetup = false;
  };

  boot = {
    loader.timeout = 0;
    kernelParams = [
      "radeon.si_support=0"
      "amdgpu.si_support=1"
      "radeon.cik_support=0"
      "amdgpu.cik_support=1"
      "quite"
      "loglevel=3"
      "systemd.show_status=auto"
      "udev.log_level=3"
      "rd.udev.log_level=3"
      "vt.global_cursor_default=0"
    ];
    consoleLogLevel = 0;
    initrd.verbose = false;
  };

  # Backlight
  hardware.brillo.enable = true;

  # Spotify
  networking.firewall = {
    allowedTCPPorts = [57621];
    allowedUDPPorts = [5353];
  };
}
