{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./services
  ];

  # AMD
  boot.initrd.kernelModules = ["amdgpu" "nvidia"];
  services.xserver = {
    enable = true;
    excludePackages = [pkgs.xterm];
    videoDrivers = ["amdgpu" "nvidia"];
  };

  # Nvidia
  hardware = {
    opengl = {
      enable = true;
      driSupport = true;
      driSupport32Bit = true;
    };

    nvidia = {
      modesetting.enable = true;
      powerManagement.enable = true;
      open = false;
      nvidiaSettings = true;
      package = config.boot.kernelPackages.nvidiaPackages.production;

      prime = {
        offload = {
          enable = true;
          enableOffloadCmd = true;
        };

        # integrated
        amdgpuBusId = "PCI:4:0:0";

        # dedicated
        nvidiaBusId = "PCI:1:0:0";
      };
    };
  };
}
