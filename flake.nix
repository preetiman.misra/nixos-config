{
  description = "Preetiman Misra's NixOS config";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";

    flake-parts.url = "github:hercules-ci/flake-parts";

    home-manager.url = "github:nix-community/home-manager";

    hyprland.url = "git+https://github.com/hyprwm/Hyprland?submodules=1";

    hyprlock.url = "github:hyprwm/hyprlock";

    stylix.url = "github:danth/stylix";

    catppuccin.url = "github:catppuccin/nix";

    ags.url = "github:Aylur/ags";

    nixvim = {
      url = "github:nix-community/nixvim";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs @ {
    self,
    nixpkgs,
    flake-parts,
    home-manager,
    hyprland,
    hyprlock,
    catppuccin,
    stylix,
    ags,
    nixvim,
    ...
  }:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = ["x86_64-linux"];

      flake = let
        inherit (nixpkgs.lib) nixosSystem;
      in {
        nixosConfigurations = {
          zeus = nixosSystem {
            specialArgs = {inherit self inputs;};
            modules = [
              ./hosts
              ./hosts/zeus

              home-manager.nixosModules.home-manager
              {
                home-manager = {
                  useGlobalPkgs = true;
                  useUserPackages = true;
                  users.bitthal = import ./home;
                  backupFileExtension = "backup";
                  extraSpecialArgs = {inherit self inputs;};
                };
              }
            ];
          };
        };
      };
    };
}
