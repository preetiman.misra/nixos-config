{stdenv}:
stdenv.mkDerivation rec {
  name = "konica-minolta-bizhub-225i-${version}";
  version = "1.0";

  src = ./.;

  installPhase = ''
    mkdir -p $out/share/cups/model/
    cp 225i.ppd $out/share/cups/model/
  '';
}
