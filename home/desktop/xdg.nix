{
  config,
  pkgs,
  ...
}: let
  cacheHome = "${config.home.homeDirectory}/.cache";
  configHome = "${config.home.homeDirectory}/.config";
  dataHome = "${config.home.homeDirectory}/.local/share";
  stateHome = "${config.home.homeDirectory}/.local/state";
in {
  home.packages = with pkgs; [
    xdg-utils
    xdg-user-dirs
  ];

  xdg.configFile."mimeapps.list".force = true;

  xdg = {
    enable = true;

    # Inherit directories
    inherit cacheHome configHome dataHome stateHome;

    mimeApps = {
      enable = true;

      defaultApplications = let
        browser = ["Chromium.desktop"];
        editor = ["nvim.desktop" "helix.desktop"];
        pdf = ["Evince.desktop"];
      in {
        "application/json" = browser;
        "application/pdf" = pdf;
        "application/x-wine-extension-ini" = editor;

        "text/html" = browser;
        "text/xml" = browser;
        "text/rss+xml" = browser;

        "audio/*" = ["mpv.desktop"];
        "video/*" = ["mpv.desktop"];
      };
    };

    userDirs = {
      enable = true;
      createDirectories = true;
      extraConfig = {
        XDG_SCREENSHOTS_DIR = "${config.xdg.userDirs.pictures}/Screenshots";
        XDG_WALLPAPERS_DIR = "${config.xdg.userDirs.pictures}/Wallpapers";
      };
    };
  };
}
