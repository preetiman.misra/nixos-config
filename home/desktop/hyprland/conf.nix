{
  pkgs,
  self,
  ...
}: let
  workspaces = builtins.concatLists (builtins.genList (
      x: let
        ws = let
          c = (x + 1) / 10;
        in
          builtins.toString (x + 1 - (c * 10));
      in [
        "$mainMod, ${ws}, workspace, ${toString (x + 1)}"
        "$mainMod SHIFT, ${ws}, movetoworkspace, ${toString (x + 1)}"
      ]
    )
    10);
in {
  wayland.windowManager.hyprland = {
    settings = {
      "exec-once" = [
        "swww-daemon"
        "swww img ${self}/wallpapers/japan.png"
        "hyprlock"
        "ags"
      ];

      monitor = [
        "eDP-1,1920x1080@120,0x0,1"
        "HDMI-A-1,1920x1080@60,1920x0,1"
      ];

      input = {
        "kb_layout" = "us";
        "follow_mouse" = 1;

        touchpad = {
          "natural_scroll" = true;
        };

        tablet = {
          output = "HDMI-A-1";
        };

        sensitivity = 0;
      };

      animations = {
        enabled = true;

        bezier = "myBezier, 0.05, 0.9, 0.1, 1.05";

        animation = [
          "windows, 1, 7, myBezier"
          "windowsOut, 1, 7, default, popin 80%"
          "border, 1, 10, default"
          "borderangle, 1, 8, default"
          "fade, 1, 7, default"
          "workspaces, 1, 6, default"
        ];
      };

      dwindle = {
        pseudotile = true;
        preserve_split = true;
      };

      master = {
        "new_is_master" = true;
      };

      gestures = {
        "workspace_swipe" = true;
      };

      windowrulev2 = [
        "float,class:^(org.wezfurlong.wezterm)$"
        "float,class:^(org.gnome.Nautilus)$"
      ];

      "$mainMod" = "SUPER";

      bind =
        [
          "$mainMod, Return, exec, kitty"
          "$mainMod, Q, killactive,"
          "$mainMod, M, exit,"
          "$mainMod, F, fullscreen,"
          "$mainMod, V, togglefloating,"
          "$mainMod, P, pseudo,"
          "$mainMod, J, togglesplit,"

          # Ags
          "$mainMod SHIFT, R, exec, ags -q" # reload ags
          "$mainMod, Space, exec, ags -t launcher"
          "$mainMod, Tab, exec, ags -t overview"
          "$mainMod, Escape, exec, ags -t powermenu"

          # Screenrecord & Screenshot
          "$mainMod SHIFT, S, exec, ags -r 'recorder.screenshot()'"
          "$mainMod SHIFT, P, exec, ags -r 'recorder.start()'"

          # Move focus with mainMod + arrow keys
          "$mainMod, left, movefocus, l"
          "$mainMod, right, movefocus, r"
          "$mainMod, up, movefocus, u"
          "$mainMod, down, movefocus, d"
        ]
        ++ workspaces;

      bindm = [
        "$mainMod, mouse:272, movewindow"
        "$mainMod, mouse:273, resizewindow"
      ];

      env = [
        "NIXOS_OZONE_WL,1"
        "MOZ_ENABLE_WAYLAND,1"
        "MOZ_WEBRENDER,1"
        "_JAVA_AWT_WM_NOREPARENTING,1"
        "QT_WAYLAND_DISABLE_WINDOWDECORATION,1"
        "QT_QPA_PLATFORM,wayland"
        "SDL_VIDEODRIVER,wayland"
        "GDK_BACKEND,wayland"
        "LIBVA_DRIVER_NAME,nvidia"
        "XDG_SESSION_TYPE,wayland"
        "GBM_BACKEND,nvidia-drm"
        "__GLX_VENDOR_LIBRARY_NAME,nvidia"
        "WLR_NO_HARDWARE_CURSORS,1"
      ];

      misc = {
        disable_splash_rendering = true;
        disable_hyprland_logo = true;
      };
    };
  };
}
