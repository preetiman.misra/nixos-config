{self, ...}: {
  # Hyprlock config
  programs.hyprlock.enable = true;

  programs.hyprlock.settings = {
    background = [
      {
        path = "${self}/wallpapers/japan.png";
        blur_size = 8;
        blur_passes = 3;
      }
    ];

    input-field = [
      {
        monitor = "eDP-1";
        size = "500, 50";
        fade_on_empty = false;
        shadow_passes = 2;
      }
    ];
  };
}
