{
  inputs,
  pkgs,
  ...
}: {
  imports = [
    inputs.hyprland.homeManagerModules.default
    ./hyprlock.nix
    ./conf.nix
  ];

  wayland.windowManager.hyprland = let
    pkg = inputs.hyprland.packages.${pkgs.system}.hyprland;
  in {
    enable = true;
    package = pkg;
    systemd.enable = true;
    xwayland.enable = true;
  };
}
