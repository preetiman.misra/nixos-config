{pkgs, ...}: {
  imports = [
    ./xdg.nix
    ./hyprland
  ];

  home.packages = with pkgs; [
    pavucontrol
    playerctl
    pulsemixer
    imv

    cava
    libva-utils
    vdpauinfo
    vulkan-tools
    glxinfo

    gamescope
    prismlauncher
    winetricks
  ];

  services = {
    playerctld.enable = true;
  };
}
