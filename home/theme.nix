{
  inputs,
  pkgs,
  ...
}: let
  cursorTheme = {
    name = "Bibata-Modern-Ice";
    size = 24;
    package = pkgs.bibata-cursors;
  };

  iconTheme = {
    name = "MoreWaita";
    package = pkgs.morewaita-icon-theme;
  };
in {
  imports = [
    inputs.catppuccin.homeManagerModules.catppuccin
  ];

  home = {
    packages = with pkgs; [
      # font
      cantarell-fonts
      noto-fonts
      noto-fonts-cjk
      noto-fonts-emoji
      liberation_ttf
      mplus-outline-fonts.githubRelease
      proggyfonts
      font-awesome
      gnome.adwaita-icon-theme
      papirus-icon-theme
    ];
    sessionVariables = {
      XCURSOR_THEME = cursorTheme.name;
      XCURSOR_SIZE = "${toString cursorTheme.size}";
    };
  };

  fonts.fontconfig.enable = true;

  qt = {
    enable = true;
    platformTheme.name = "kde";
  };
}
