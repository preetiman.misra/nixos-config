{pkgs, ...}: {
  imports = [
    ./theme.nix
    ./stylix.nix
    ./editors
    ./desktop
    ./applications
    ./services
  ];

  home = {
    username = "bitthal";
    homeDirectory = "/home/bitthal";
    stateVersion = "24.05";
  };

  programs.git = {
    enable = true;
    userName = "Preetiman Misra";
    userEmail = "preetiman.misra@gmail.com";

    extraConfig = {
      init.defaultBranch = "main";
    };
  };

  programs.home-manager.enable = true;
}
