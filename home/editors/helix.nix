{
  self,
  config,
  pkgs,
  ...
}: {
  # Helix
  programs.helix = {
    enable = true;

    extraPackages = with pkgs; [
      # Nix
      alejandra
      nil

      # Rust
      cargo
      rust-analyzer
      clippy

      # php
      nodePackages_latest.intelephense

      # Deno, typescript and the web
      deno
      nodePackages_latest.typescript-language-server
      vscode-langservers-extracted
      tailwindcss-language-server
      emmet-ls
      dprint
      nodePackages.prettier

      # Svelte
      nodePackages_latest.svelte-language-server

      # C Sharp
      csharp-ls
    ];

    settings = {
      editor = {
        soft-wrap.enable = true;
        line-number = "relative";
        cursor-shape = {
          insert = "bar";
          normal = "block";
          select = "underline";
        };
        file-picker.hidden = false;
      };
    };

    languages = {
      # Language servers
      language-server = {
        # Nil
        nil = {
          command = "${pkgs.nil}/bin/nil";
          config.nil = {
            formatting.command = ["${pkgs.alejandra}/bin/alejandra" "-q"];
            nix.autoEvalInputs = true;
          };
        };

        # Rust Analyzer
        rust-analyzer = {
          command = "${pkgs.rust-analyzer}/bin/rust-analyzer";
          config.rust-analyzer = {
            cargo.loadOutDirsFromCheck = true;
            cargo.autoreload = true;
            procMacro.enable = true;
            lens = {
              references = true;
              methodReferences = true;
            };
            check.command = ["${pkgs.clippy}/bin/clippy"];
            checkOnSave = true;
            completion.autoimport.enable = true;
            experimental.procAttrMacros = true;
          };
        };

        # Emmet
        emmet-ls = {
          command = "${pkgs.emmet-ls}/bin/emmet-ls";
          args = ["--stdio"];
        };

        # vscode-eslint
        eslint = {
          command = "${pkgs.vscode-langservers-extracted}/bin/vscode-eslint-language-server";
          args = ["--stdio"];
          config.eslint = {
            codeActionsOnSave = {
              mode = "all";
              "source.fixAll.eslint" = true;
            };
            format.enable = true;
            nodePath = "";
            quiet = false;
            rulesCustomizations = [];
            run = "onType";
            validate = "on";
            experimental = {};
            problems = {
              shortenToSingleLine = false;
            };
            codeAction = {
              disableRuleComment = {
                enable = true;
                location = "separateLine";
              };
              showDocumentation = {
                enable = true;
              };
            };
          };
        };

        # json
        vscode-json-language-server.config = {
          json = {
            validate = {
              enable = true;
              format.enable = true;
            };
          };
          provideFormatter = true;
        };

        # css
        vscode-css-language-server.config = {
          css = {
            validate = {
              enable = true;
            };
          };
          scss = {
            validate = {
              enable = true;
            };
          };
          provideFormatter = true;
        };

        # svelte-language-server
        svelteserver = {
          command = "${pkgs.nodePackages_latest.svelte-language-server}/bin/svelte-language-server";
          args = ["--stdio"];
        };

        # deno
        deno = {
          command = "${pkgs.deno}/bin/deno";
          args = ["lsp"];
          config.deno = {
            enable = true;
          };
        };

        # csharp
        csharp-ls = {
          command = "${pkgs.csharp-ls}/bin/csharp-ls";
        };
      };

      # Languages
      language = [
        # Nix
        {
          name = "nix";
          auto-format = true;
          language-servers = ["nil"];
        }

        # Rust
        {
          name = "rust";
          file-types = ["rs"];
          auto-format = true;
          indent = {
            tab-width = 4;
            unit = " ";
          };
          formatter = {command = "${pkgs.cargo}/bin/cargo fmt";};
          language-servers = ["rust-analyzer"];
        }

        # Typescript
        {
          name = "typescript";
          language-id = "typescript";
          scope = "source.ts";
          injection-regex = "^(ts|typescript)$";
          file-types = ["ts"];
          language-servers = ["deno" "eslint" "emmet-ls"];
          roots = ["deno.json" "deno.jsonc" "package.json"];
          formatter = {
            command = "${pkgs.dprint}/bin/dprint";
            args = ["fmt" "--stdin" "typescript"];
          };
          auto-format = true;
        }

        # tsx
        {
          name = "tsx";
          language-id = "tsx";
          file-types = ["tsx"];
          language-servers = ["deno" "eslint" "emmet-ls"];
          roots = ["deno.json" "deno.jsonc" "package.json"];
          formatter = {
            command = "${pkgs.dprint}/bin/dprint";
            args = ["fmt" "--stdin" "tsx"];
          };
          auto-format = true;
        }

        # Typescript
        {
          name = "javascript";
          language-id = "javascript";
          scope = "source.js";
          injection-regex = "^(js|jypescript)$";
          file-types = ["js"];
          language-servers = ["deno" "eslint" "emmet-ls"];
          roots = ["deno.json" "deno.jsonc" "package.json"];
          formatter = {
            command = "${pkgs.dprint}/bin/dprint";
            args = ["fmt" "--stdin" "javascript"];
          };
          auto-format = true;
        }

        # jsx
        {
          name = "jsx";
          language-id = "jsx";
          file-types = ["jsx"];
          language-servers = ["deno" "eslint" "emmet-ls"];
          roots = ["deno.json" "deno.jsonc" "package.json"];
          formatter = {
            command = "${pkgs.dprint}/bin/dprint";
            args = ["fmt" "--stdin" "jsx"];
          };
          auto-format = true;
        }

        # html
        {
          name = "html";
          language-servers = ["vscode-html-language-server" "emmet-ls" "tailwindcss-language-server"];
          formatter = {
            command = "prettier";
            args = ["--parser" "html"];
          };
          auto-format = true;
        }

        # css
        {
          name = "css";
          language-servers = ["vscode-css-language-server" "emmet-ls" "tailwindcss-language-server"];
          formatter = {
            command = "prettier";
            args = ["--parser" "css"];
          };
          auto-format = true;
        }

        # Svelte
        {
          name = "svelte";
          language-servers = ["svelteserver"];
          auto-format = true;
        }

        # php
        {
          name = "php";
          language-servers = ["intelephense" "emmet-ls"];
          auto-format = true;
        }

        # csharp
        {
          name = "c-sharp";
          file-types = ["cs" "razor"];
          language-servers = ["csharp-ls"];
          auto-format = true;
        }
      ];
    };
  };
}
