{
  pkgs,
  inputs,
  ...
}: {
  imports = [inputs.nixvim.homeManagerModules.nixvim];

  # For telescope live_grep
  home.packages = with pkgs; [
    ripgrep

    # Formatters
    black
    codespell
    csharpier
    alejandra
    gotools
    gofumpt
    just
    gleam
    nodePackages.prettier
    csharpier
    ocamlPackages.ocamlformat_0_26_1
  ];

  programs.nixvim = {
    enable = true;
    enableMan = true;
    defaultEditor = true;

    # Colorscheme
    colorschemes.catppuccin = {
      enable = true;
      settings.flavour = "macchiato";
    };

    # Global Options
    globals = {
      mapleader = " ";
      maplocalleader = " ";
      "have_nerd_font" = true;
    };

    # Options
    opts = {
      number = true;
      relativenumber = true;
      mouse = "a";
      showmode = false;
      expandtab = true;
      shiftwidth = 2;
      tabstop = 2;
      softtabstop = 2;
      clipboard = "unnamedplus";
      breakindent = true;
      undofile = true;
      ignorecase = true;
      smartcase = true;
      signcolumn = "yes";
      updatetime = 250;
      timeoutlen = 300;
      splitright = true;
      splitbelow = true;
      list = true;
      listchars = {
        tab = "» ";
        trail = "·";
        nbsp = "␣";
      };
      inccommand = "split";
      cursorline = true;
      scrolloff = 10;
      hlsearch = true;
    };

    # Keymaps
    keymaps = [
      {
        mode = "n";
        key = "<Esc>";
        action = "<cmd>nohlsearch<CR>";
      }

      # Diagnostic keymaps
      {
        mode = "n";
        key = "[d";
        action = "vim.diagnostic.goto_prev";
        options = {
          desc = "Go to previous [D]iagnostic message";
        };
      }
      {
        mode = "n";
        key = "]d";
        action = "vim.diagnostic.goto_next";
        options = {
          desc = "Go to next [D]iagnostic message";
        };
      }
      {
        mode = "n";
        key = "<leader>e";
        action = "vim.diagnostic.open_float";
        options = {
          desc = "Show diagnostic [E]rror message";
        };
      }
      {
        mode = "n";
        key = "<leader>q";
        action = "vim.diagnostic.setloclist";
        options = {
          desc = "Show diagnostic [Q]uickfix list";
        };
      }

      # Split navigation
      {
        mode = "n";
        key = "<C-h>";
        action = "<C-w><C-h>";
        options = {
          desc = "Move focus to the left window";
        };
      }
      {
        mode = "n";
        key = "<C-l>";
        action = "<C-w><C-l>";
        options = {
          desc = "Move focus to the right window";
        };
      }
      {
        mode = "n";
        key = "<C-j>";
        action = "<C-w><C-j>";
        options = {
          desc = "Move focus to the lower window";
        };
      }
      {
        mode = "n";
        key = "<C-k>";
        action = "<C-w><C-k>";
        options = {
          desc = "Move focus to the upper window";
        };
      }

      # Neotree
      {
        key = "<C-n>";
        action = "<cmd>Neotree toggle<CR>";
        options = {
          silent = true;
          desc = "Toggle neo-tree";
        };
      }
    ];

    # Augroups
    autoGroups = {
      "kickstart-highlight-yank" = {
        clear = true;
      };
      "conform-format" = {
        clear = true;
      };
    };

    # Autocmds
    autoCmd = [
      # Highlight after yank
      {
        event = ["TextYankPost"];
        desc = "Highlight when yanking (copying) text";
        group = "kickstart-highlight-yank";
        callback = {__raw = "function() vim.highlight.on_yank() end";};
      }

      # Conform
      {
        event = ["BufWritePre"];
        desc = "Auto-format using conform-nvim";
        group = "conform-format";
        callback = {__raw = "function(args) require('conform').format({ bufnr = args.buf }) end";};
      }
    ];

    plugins = {
      transparent = {
        enable = true;
      };

      neo-tree = {
        enable = true;
        autoCleanAfterSessionRestore = true;
      };

      lualine = {
        enable = true;
        componentSeparators = {
          right = "|";
        };
        sectionSeparators = {
          right = "";
        };
        sections = {
          lualine_a = [
            {
              name = "mode";
              separator.right = "";
            }
          ];
          lualine_b = [
            {
              name = "branch";
            }
            {
              name = "diff";
              separator.right = "";
            }
            {
              name = "diagnostics";
              separator.right = "";
            }
          ];
        };
      };

      comment = {
        enable = true;
        settings.mappings = {
          basic = true;
          extra = true;
        };
      };

      gitsigns = {
        enable = true;
        settings = {
          signs = {
            add = {text = "+";};
            change = {text = "~";};
            delete = {text = "_";};
            topdelete = {text = "‾";};
            changedelete = {text = "~";};
          };
        };
      };

      telescope = {
        enable = true;
        extensions = {
          fzf-native.enable = true;
          ui-select.enable = true;
        };
        keymaps = {
          "<leader>ff" = "find_files";
          "<leader>fg" = "live_grep";
        };
      };

      # Grammar
      treesitter.enable = true;

      # LSP
      lsp = {
        enable = true;

        keymaps = {
          lspBuf = {
            K = "hover";
            gD = "references";
            gd = "definition";
          };
        };

        servers = {
          tsserver = {
            enable = true;
            filetypes = ["javascript" "javascriptreact" "typescript" "typescriptreact" "vue"];
            settings = {
              init_options = {
                plugins = [
                  {
                    name = "@vue/typescript-plugin";
                    location = "/usr/local/lib/node_modules/@vue/typescript-plugin";
                    languages = ["javascript" "typescript" "vue"];
                  }
                ];
              };
            };
          };
          emmet-ls = {
            enable = true;
            filetypes = [
              "html"
              "templ"
              "php"
              "razor"
              "astro"
              "javascriptreact"
              "typescriptreact"
              "svelte"
              "vue"
            ];
          };
          tailwindcss = {
            enable = true;
            filetypes = [
              "html"
              "templ"
              "php"
              "astro"
              "razor"
              "javascriptreact"
              "typescriptreact"
              "vue"
              "svelte"
            ];
          };
          svelte = {
            enable = true;
            package = pkgs.nodePackages.svelte-language-server;
          };
          volar.enable = true;

          nil-ls.enable = true;

          pyright.enable = true;

          csharp-ls.enable = true;

          rust-analyzer = {
            enable = true;
            installRustc = true;
            installCargo = true;
          };

          gopls.enable = true;
          templ.enable = true;

          zls.enable = true;

          gleam.enable = true;

          ocamllsp.enable = true;
        };
      };
      conform-nvim = {
        enable = true;
        formattersByFt = {
          nix = ["alejandra"];
          go = ["goimports" "gofumpt"];
          javascript = ["prettier"];
          jsx = ["prettier"];
          typescript = ["prettier"];
          vue = ["prettier"];
          tsx = ["prettier"];
          just = ["just"];
          cs = ["csharpier"];
          py = ["black"];
          gleam = ["gleam"];
          ocaml = ["ocamlformat"];
        };
      };
      lspsaga = {
        enable = true;
      };
      lspkind.enable = true;

      # Completions
      cmp = {
        enable = true;
        autoEnableSources = true;
        settings = {
          sources = [
            {name = "nvim_lsp";}
            {name = "luasnip";}
            {name = "path";}
            {name = "buffer";}
          ];
          snippet.expand = ''
            function(args)
              require('luasnip').lsp_expand(args.body)
            end
          '';
          mapping = {
            "<C-Space>" = "cmp.mapping.complete()";
            "<C-d>" = "cmp.mapping.scroll_docs(-4)";
            "<C-e>" = "cmp.mapping.close()";
            "<C-f>" = "cmp.mapping.scroll_docs(4)";
            "<CR>" = "cmp.mapping.confirm({ select = true })";
            "<S-Tab>" = "cmp.mapping(cmp.mapping.select_prev_item(), {'i', 's'})";
            "<Tab>" = "cmp.mapping(cmp.mapping.select_next_item(), {'i', 's'})";
          };
        };
      };
      luasnip.enable = true;
    };
  };
}
