{
  inputs,
  config,
  pkgs,
  ...
}: let
  nerdfonts = pkgs.nerdfonts.override {
    fonts = [
      "Ubuntu"
      "UbuntuMono"
      "JetBrainsMono"
      "CascadiaCode"
      "FiraCode"
      "Mononoki"
    ];
  };
in {
  imports = [inputs.stylix.homeManagerModules.stylix];

  stylix = {
    image = config.lib.stylix.pixel "base0A";

    base16Scheme = "${pkgs.base16-schemes}/share/themes/catppuccin-mocha.yaml";

    polarity = "dark";

    autoEnable = true;

    fonts = {
      sansSerif = {
        name = "Ubuntu Nerd Font";
        package = nerdfonts;
      };
      serif = config.stylix.fonts.sansSerif;
      monospace = {
        name = "UbuntuMono Nerd Font";
        package = nerdfonts;
      };
      emoji = {
        package = pkgs.noto-fonts-emoji;
        name = "Noto Color Emoji";
      };
    };

    cursor = {
      package = pkgs.bibata-cursors;
      name = "Bibata-Modern-Ice";
      size = 24;
    };

    targets = {
      vscode.enable = false;
    };
  };
}
