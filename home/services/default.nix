{
  inputs,
  pkgs,
  ...
}: {
  imports = [
    ./wlogout.nix

    inputs.ags.homeManagerModules.default
  ];

  # AGS dependencies
  home.packages = with pkgs; [
    bun
    bat
    eza
    dart-sass
    which
    fd
    fzf
    brightnessctl
    swww
    (mpv.override {scripts = [mpvScripts.mpris];})
    transmission_4-gtk
    inputs.hyprland.packages.${system}.default
    slurp
    wf-recorder
    wl-clipboard
    wayshot
    swappy
    hyprpicker
    pavucontrol
    networkmanager
    gtk3
  ];

  # ags
  programs.ags = {
    enable = true;
    extraPackages = with pkgs; [
      gtksourceview
      webkitgtk
      accountsservice
    ];
    configDir = ./ags;
  };

  # dconf
  dconf.settings = {
    "org/virt-manager/virt-manager/connections" = {
      autoconnect = ["qemu:///system"];
      uris = ["qemu:///system"];
    };
  };
}
