import gtk from "./gtk";
import hyprland from "./hyprland";
import lowBattery from "./battery";
import notifications from "./notifications";

export default function init() {
  try {
    gtk();
    lowBattery();
    notifications();
    hyprland();
  } catch (error) {
    logError(error);
  }
}
