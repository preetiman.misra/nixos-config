{
  programs.bash.enable = true;

  # ZSH
  programs.zsh = {
    enable = true;
    enableCompletion = true;
    antidote = {
      enable = true;
      plugins = [
        "zsh-users/zsh-syntax-highlighting"
        "zsh-users/zsh-autosuggestions"
        "zsh-users/zsh-history-substring-search"
      ];
    };
    shellAliases = {
      ll = "ls -l";
      e = "nvim";
      h = "hx";
      g = "git";
      ga = "git add";
      gc = "git commit";
      gp = "git push";
    };
  };

  # Starship
  programs.starship = {
    enable = true;
    enableNushellIntegration = true;
    settings = {
      character = {
        success_symbol = "[❅](bold green)";
        error_symbol = "[❅](bold red)";
      };
    };
  };

  # Zellij
  programs.zellij = {
    enable = true;
  };
}
