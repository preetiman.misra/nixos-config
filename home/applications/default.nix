{
  config,
  pkgs,
  ...
}: {
  imports = [
    ./shell.nix
    ./kitty.nix
  ];

  home.packages = with pkgs;
  with gnome; [
    chromium
    libreoffice-fresh
    whatsapp-for-linux
    thunderbird
    krita
    spotify
    toybox
    ffmpeg_7-full

    # dev
    openssl_3_3
    busybox
    inetutils
    #python
    python312
    python312Packages.conda
    #golang
    go
    gotools
    go-tools
    #node
    nodejs_20
    #php
    php
    phpPackages.composer
    #deploy
    railway
    flyctl
    #code-editors
    jetbrains.phpstorm
    # gleam
    gleam
    erlang_27
    rebar3
    #ocml
    ocaml
    opam

    # Gnome
    loupe
    nautilus
    baobab
    evince
    gnome-text-editor
    gnome-secrets
    gnome-calendar
    gnome-boxes
    gnome-system-monitor
    gnome-control-center
    gnome-weather
    gnome-calculator
    gnome-clocks
    gnome-software # for flatpak
    gnome-tweaks
    gnome-themes-extra
  ];

  home.sessionVariables = {
    DOTNET_ROOT = "${pkgs.dotnetCorePackages.dotnet_8.sdk}";
  };

  programs.firefox = {
    enable = true;
  };

  # VSCode
  programs.vscode = {
    enable = true;
    package = pkgs.vscode.fhsWithPackages (ps: with ps; [rustup zlib openssl.dev pkg-config]);
  };

  # Direnv
  programs.direnv = {
    enable = true;
    enableNushellIntegration = true;
    nix-direnv.enable = true;
  };
}
