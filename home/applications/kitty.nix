{lib, ...}: {
  # Kitty
  programs.kitty = {
    enable = true;
    font = {
      name = lib.mkForce "JetBrainsMono Nerd Font";
      size = lib.mkForce 16;
    };
    settings = {
      window_padding_width = "0 10";
      enabled_layout = "*";
      tab_bar_edge = "bottom";
      tab_bar_style = "powerline";

      confirm_os_window_close = 0;
    };
  };
}
